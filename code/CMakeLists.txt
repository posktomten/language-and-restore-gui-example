cmake_minimum_required(VERSION 3.16)
project(test VERSION 1.0 LANGUAGES CXX)
set(QT_MINIMUM_VERSION 6.4.0)

set(CMAKE_INCLUDE_CURRENT_DIR ON)
set(CMAKE_AUTOUIC ON)

find_package(QT NAMES Qt6 REQUIRED COMPONENTS Core LinguistTools)
find_package(Qt${QT_VERSION_MAJOR} REQUIRED COMPONENTS Gui LinguistTools)
find_package(Qt${QT_VERSION_MAJOR} OPTIONAL_COMPONENTS Widgets)

SET(CMAKE_RUNTIME_OUTPUT_DIRECTORY ${CMAKE_CURRENT_SOURCE_DIR}/../build-executable6)

qt_standard_project_setup()

set(TS_FILES
    ${CMAKE_CURRENT_SOURCE_DIR}/i18n/test_es_ES.ts
    ${CMAKE_CURRENT_SOURCE_DIR}/i18n/test_fa_IR.ts
    ${CMAKE_CURRENT_SOURCE_DIR}/i18n/test_sv_SE.ts
)

set(SOURCE_FILES
    main.cpp
    mainwindow.cpp
    mainwindow.h
    mainwindow.ui
)

qt_add_lupdate(test TS_FILES ${TS_FILES})

qt_add_executable(test WIN32 MACOSX_BUNDLE
    ${SOURCE_FILES}
)


target_link_libraries(test PRIVATE
    Qt::Core
    Qt::Gui
)


# Resources:
set(resurs_resource_files
    "i18n/test_es_ES.qm"
    "i18n/test_fa_IR.qm"
    "i18n/test_sv_SE.qm"
    "images/english.png"
    "images/iran.png"
    "images/spain.png"
    "images/swedish.png"
)

qt_add_resources(test "resurs"
    PREFIX
        "/"
    FILES
        ${resurs_resource_files}
)

if((QT_VERSION_MAJOR GREATER 4))
    target_link_libraries(test PRIVATE
        Qt::Widgets
    )
endif()

install(TARGETS test
    BUNDLE DESTINATION .
    RUNTIME DESTINATION ${CMAKE_INSTALL_BINDIR}
)

#qt_generate_deploy_app_script(
#    TARGET test
#    FILENAME_VARIABLE deploy_script
#    NO_UNSUPPORTED_PLATFORM_ERROR
#)
#install(SCRIPT ${deploy_script})
