# Language and restore GUI example
 
Sample project to show how to save and restore window position and window size settings. <br>
And to show a way to handle translations.<br>

For Linux and Windows, Qt5 and Qt6.<br>
Can be used with qmake or cmake.

