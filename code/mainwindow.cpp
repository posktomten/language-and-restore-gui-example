#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QSettings>
#include <QProcess>
#include <QDebug>

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    ui->pbQuit->setText(tr("Close"));
    ui->textEdit->setText(tr("Hello World!"));
    QSettings settings(QSettings::IniFormat, QSettings::UserScope, QStringLiteral(u"TEST"), QStringLiteral(u"test"));
    // Remember GUI settings
    settings.beginGroup(QStringLiteral(u"MainWindow"));
    this->restoreGeometry(settings.value("savegeometry").toByteArray());
    this->restoreGeometry(settings.value("savestate").toByteArray());
    settings.endGroup();
    connect(ui->pbQuit, &QPushButton::clicked, [this]()->void {
        close();
    });
    // Click on "Spanish"
    connect(ui->pbSpanish, &QPushButton::clicked, [this]() -> void {

        QSettings settings(QSettings::IniFormat, QSettings::UserScope, QStringLiteral(u"TEST"), QStringLiteral(u"test"));
        // Saves the language selection in QSettings (AppData\Roaming\TEST)
        settings.beginGroup(QStringLiteral(u"Language"));
        settings.setValue("language", "es_ES");
        settings.endGroup();
        restart();

    });
    // Click on "Swedish"
    connect(ui->pbSwedish, &QPushButton::clicked, [this]() -> void {

        QSettings settings(QSettings::IniFormat, QSettings::UserScope, QStringLiteral(u"TEST"), QStringLiteral(u"test"));
        settings.beginGroup(QStringLiteral(u"Language"));
        // Saves the language selection in QSettings (AppData\Roaming\TEST)
        settings.setValue("language", "sv_SE");
        settings.endGroup();
        restart();
    });
    // Click on "English"
    connect(ui->pbEnglish, &QPushButton::clicked, [this]() -> void {

        QSettings settings(QSettings::IniFormat, QSettings::UserScope, QStringLiteral(u"TEST"), QStringLiteral(u"test"));
        settings.beginGroup(QStringLiteral(u"Language"));
        // Saves the language selection in QSettings (AppData\Roaming\TEST)
        settings.setValue("language", "en_US");
        settings.endGroup();
        // The program is restarted to load the new language settings.
        restart();
    });
    // Click on "Persian"
    connect(ui->pbParsian, &QPushButton::clicked, [this]() -> void {
        QSettings settings(QSettings::IniFormat, QSettings::UserScope, QStringLiteral(u"TEST"), QStringLiteral(u"test"));
        settings.beginGroup(QStringLiteral(u"Language"));
        // Saves the language selection in QSettings (AppData\Roaming\TEST)
        settings.setValue("language", "fa_IR");
        settings.endGroup();
        // The program is restarted to load the new language settings.
        restart();
    });
}

void MainWindow::restart()
{
    // Closes and starts the program
    const QString executable = QCoreApplication::applicationFilePath();
    QProcess p;
    p.setProgram(executable);
    close();
    p.startDetached();
}
void MainWindow::setEndConfig()
{
    QSettings settings(QSettings::IniFormat, QSettings::UserScope, QStringLiteral(u"TEST"), QStringLiteral(u"test"));
    settings.beginGroup(QStringLiteral(u"MainWindow"));
    settings.setValue("savegeometry", this->saveGeometry());
    settings.setValue("savestate", this->saveState());
    settings.endGroup();
}


MainWindow::~MainWindow()
{
    delete ui;
}

