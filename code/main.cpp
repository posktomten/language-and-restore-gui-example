#include "mainwindow.h"

#include <QApplication>
#include <QLocale>
#include <QTranslator>
#include <QSettings>
#include <QDebug>
int main(int argc, char *argv[])
{
    QApplication *a = new QApplication(argc, argv);
    QString currentLocale(QLocale::system().name());
    QSettings settings(QSettings::IniFormat, QSettings::UserScope, QStringLiteral(u"TEST"),
                       QStringLiteral(u"test"));
    settings.beginGroup("Language");
    QString language = settings.value("language", currentLocale).toString();
    settings.endGroup();
    const QString qttranslationsPath(QStringLiteral(u":/i18n/"));
    QTranslator qtTranslator;

    if(qtTranslator.load(qttranslationsPath + QStringLiteral(u"test") + QStringLiteral(u"_") + language + QStringLiteral(u".qm"))) {
        a->installTranslator(&qtTranslator);
    }

    MainWindow *w = new MainWindow;
    QObject::connect(a, &QCoreApplication::aboutToQuit,
                     [w]() -> void { w->setEndConfig(); });
    w->show();
    return a->exec();
}
