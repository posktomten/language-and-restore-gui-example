/********************************************************************************
** Form generated from reading UI file 'mainwindow.ui'
**
** Created by: Qt User Interface Compiler version 6.4.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_MAINWINDOW_H
#define UI_MAINWINDOW_H

#include <QtCore/QVariant>
#include <QtGui/QIcon>
#include <QtWidgets/QApplication>
#include <QtWidgets/QFormLayout>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QMenuBar>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QStatusBar>
#include <QtWidgets/QTextEdit>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_MainWindow
{
public:
    QWidget *centralwidget;
    QFormLayout *formLayout;
    QHBoxLayout *horizontalLayout;
    QPushButton *pbSwedish;
    QPushButton *pbEnglish;
    QPushButton *pbSpanish;
    QPushButton *pbParsian;
    QPushButton *pbQuit;
    QTextEdit *textEdit;
    QMenuBar *menubar;
    QStatusBar *statusbar;

    void setupUi(QMainWindow *MainWindow)
    {
        if (MainWindow->objectName().isEmpty())
            MainWindow->setObjectName("MainWindow");
        MainWindow->resize(730, 660);
        centralwidget = new QWidget(MainWindow);
        centralwidget->setObjectName("centralwidget");
        formLayout = new QFormLayout(centralwidget);
        formLayout->setObjectName("formLayout");
        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setObjectName("horizontalLayout");
        pbSwedish = new QPushButton(centralwidget);
        pbSwedish->setObjectName("pbSwedish");
        pbSwedish->setText(QString::fromUtf8("Svenska"));
        QIcon icon;
        icon.addFile(QString::fromUtf8(":/images/swedish.png"), QSize(), QIcon::Normal, QIcon::Off);
        pbSwedish->setIcon(icon);

        horizontalLayout->addWidget(pbSwedish);

        pbEnglish = new QPushButton(centralwidget);
        pbEnglish->setObjectName("pbEnglish");
        pbEnglish->setText(QString::fromUtf8("English"));
        QIcon icon1;
        icon1.addFile(QString::fromUtf8(":/images/english.png"), QSize(), QIcon::Normal, QIcon::Off);
        pbEnglish->setIcon(icon1);

        horizontalLayout->addWidget(pbEnglish);

        pbSpanish = new QPushButton(centralwidget);
        pbSpanish->setObjectName("pbSpanish");
        pbSpanish->setText(QString::fromUtf8("Espa\303\261ol"));
        QIcon icon2;
        icon2.addFile(QString::fromUtf8(":/images/spain.png"), QSize(), QIcon::Normal, QIcon::Off);
        pbSpanish->setIcon(icon2);

        horizontalLayout->addWidget(pbSpanish);

        pbParsian = new QPushButton(centralwidget);
        pbParsian->setObjectName("pbParsian");
        pbParsian->setText(QString::fromUtf8("\331\201\330\247\330\261\330\263\333\214"));
        QIcon icon3;
        icon3.addFile(QString::fromUtf8(":/images/iran.png"), QSize(), QIcon::Normal, QIcon::Off);
        pbParsian->setIcon(icon3);

        horizontalLayout->addWidget(pbParsian);

        pbQuit = new QPushButton(centralwidget);
        pbQuit->setObjectName("pbQuit");

        horizontalLayout->addWidget(pbQuit);


        formLayout->setLayout(1, QFormLayout::LabelRole, horizontalLayout);

        textEdit = new QTextEdit(centralwidget);
        textEdit->setObjectName("textEdit");
        QFont font;
        font.setPointSize(22);
        textEdit->setFont(font);

        formLayout->setWidget(0, QFormLayout::SpanningRole, textEdit);

        MainWindow->setCentralWidget(centralwidget);
        menubar = new QMenuBar(MainWindow);
        menubar->setObjectName("menubar");
        menubar->setGeometry(QRect(0, 0, 730, 26));
        MainWindow->setMenuBar(menubar);
        statusbar = new QStatusBar(MainWindow);
        statusbar->setObjectName("statusbar");
        MainWindow->setStatusBar(statusbar);

        retranslateUi(MainWindow);

        QMetaObject::connectSlotsByName(MainWindow);
    } // setupUi

    void retranslateUi(QMainWindow *MainWindow)
    {
        MainWindow->setWindowTitle(QCoreApplication::translate("MainWindow", "MainWindow", nullptr));
        pbQuit->setText(QString());
    } // retranslateUi

};

namespace Ui {
    class MainWindow: public Ui_MainWindow {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MAINWINDOW_H
