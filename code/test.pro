QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

CONFIG += c++11

# You can make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += \
    main.cpp \
    mainwindow.cpp

HEADERS += \
    mainwindow.h

FORMS += \
    mainwindow.ui

#https://invent.kde.org/qt/qt/qttranslations
TRANSLATIONS += \
    i18n/test_sv_SE.ts \
    i18n/test_es_ES.ts \
    i18n/test_fa_IR.ts

UI_DIR = ../code

TARGET = test

CONFIG += lrelease
#CONFIG += embed_translations


# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

RESOURCES += \
    resurs.qrc

equals(QT_MAJOR_VERSION, 5) {
DESTDIR="../build-executable5"
}

equals(QT_MAJOR_VERSION, 6) {
DESTDIR="../build-executable6"
}
